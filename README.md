# jakesys/aws

**Small, simple Alpine Linux based Docker container for AWS CLI v1.**

Based on the official [`alpine:latest` image](https://hub.docker.com/_/alpine) and the latest `aws-cli` package available via `pip3`.

https://gitlab.com/jakesys/aws is the authoritative repository for this project; its CI/CD pipeline is triggered daily to build, test, and push the most recent AWS CLI v1 release to [`jakesys/aws` on Docker Hub](https://hub.docker.com/r/jakesys/aws).

_For AWS CLI v2, see https://gitlab.com/jakesys/aws2 and [`jakesys/aws2` on Docker Hub](https://hub.docker.com/r/jakesys/aws2)._

## Usage
```
docker run [<docker-opts>] -v <aws-config-dir>:/root/.aws \
  jakesys/aws [--log | --logfmt <fmt>] [<aws-opts>] <cmd> [<cmd-opts>]
```
### Extra Options

#### `--log`
Outputs log entries on STDERR, which start with a timestamp and the container's `hostname`.  Useful when using the 'awslog' logging driver and you prefer not to have *every* line of output be its own CloudWatch log entry.  You'll need to use the `awslogs-datetime-format='%Y-%m-%dT%H:%M:%SZ - '` awslog log option to get CloudWatch to parse log entries properly.

* START log entry contains the `aws` commandline being executed.
* OUTPUT log entry contains the interleaved STDOUT and STDERR from the executed commandline.
* EXIT log entry contains the commandline's exit code and duration, in seconds.

#### `--logfmt <fmt>`
Outputs log entries on STDERR, prefixed with a strftime(3)-compatible format string.  To include the `aws` container's `hostname`, include `@{hostname}` in the format string.  For example, the default `--log` format is equivalent to `--logfmt '%FT%TZ - @{hostname} - '`.
