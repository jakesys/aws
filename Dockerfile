FROM alpine:latest
MAINTAINER Jake Buchholz <jakesys@jakesys.net>

COPY root /
RUN /bootstrap.sh

ENTRYPOINT ["/entry"]
CMD ["--version"]

VOLUME ["/root/.aws"]
