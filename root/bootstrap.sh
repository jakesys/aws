#!/bin/sh

set -ex

# install packages
apk upgrade --no-cache
apk add --no-cache groff py3-pip
pip3 install --no-cache-dir awscli --break-system-packages

# set .version
echo "$(aws --version 2>&1 | cut -d/ -f2 | cut -d' ' -f1)" > /.version

# clean up
rm -rf /bootstrap.sh /var/lib/apk/* /var/tmp/*
